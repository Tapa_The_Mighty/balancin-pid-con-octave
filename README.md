<h1>PID con Octave</h1>
<p>&nbsp;</p>
<p><img src="img/img_01.png" alt="" /></p>
<p>En este repositorio se encuentra una interfaz gr&aacute;fica para usar controlar un balancin PID, con GNU Octave. La misma aplicaci&oacute;n se puede editar con el siguiente programa:</p>
<ul>
<li><a href="https://gitlab.com/labinformatica/guieditor">guiEditor</a></li>
</ul>
