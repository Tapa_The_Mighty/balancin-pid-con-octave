
%   aNanoPidServoOffset(obj, nroServo, nroPin)
%   obj: corresponde con el que se establece la comunicación.
%   Offset: offset de ajuste para el servo.
%   El paquete de datos, para pines digitales, se construye como:
%   0xB0, CMD = 15, offset ,0, 0, 0, 0, 0 , 0x0b


function ret = aNanoPidServoOffset(obj, offset)
  v = [176, 15, offset, 0, 0, 0, 0, 0, 11];

  v(8) = 0;
  for i=1:7
    v(8) = v(8) + v(i);
  endfor
  v(8) = mod(v(8), 256);
  port = obj.serialDev;
  srl_flush(port);
  for i=1:9
    srl_write(port, uint8(v(i)));
  endfor
%  ret =  srl_read(port, 1);
endfunction
