
%   aNanoPidServoAttach(obj, nroServo, nroPin)
%   obj: corresponde con el que se establece la comunicación.
%   nroServo: índice del servo a utilizar. Valores válidos van de 0 a 3 inclusive.
%   nroPin: identificación del pin al que se conectará el servo.
%   El paquete de datos, para pines digitales, se construye como:
%   0xB0, CMD = 13, nroPin,0, 0, 0, 0, suma, 0x0b


function ret = aNanoPidServoAttach(obj, nroPin)
  v = [176, 13, nroPin, 0, 0, 0, 0, 0, 11];

  v(8) = 0;
  for i=1:7
    v(8) = v(8) + v(i);
  endfor
  v(8) = mod(v(8), 256);
  port = obj.serialDev;
  srl_flush(port);
  for i=1:9
    srl_write(port, uint8(v(i)));
  endfor
%  ret =  srl_read(port, 1);
endfunction
