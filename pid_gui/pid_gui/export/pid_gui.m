## -*- texinfo -*-
## @deftypefn  {} {@var{ret} =} pid_gui ()
##
## Create and show the main dialog, return a struct as representation of dialog.
##
## @end deftypefn
function ret = pid_gui()
  [dir, name, ext] = fileparts( mfilename('fullpathext') );
  global _pid_guiBasePath = dir;
  global _pid_guiImgPath = [dir filesep() 'img'];
  addpath( [dir filesep() "img" ]);
  addpath( [dir filesep() "fcn" ]);
  addpath( [dir filesep() "libs" ]);
  addpath( [dir filesep() "wnd" ]);
  mainDlg();
end
