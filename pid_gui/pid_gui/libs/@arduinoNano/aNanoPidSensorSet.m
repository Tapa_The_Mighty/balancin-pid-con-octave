
%   aNanoPidSensorSet(obj,Op,0,0)
%   El paquete de datos, para establecer estos valores se construye como:
%   obj: corresponde con el que se establece la comunicación.
%   Op: Tipo de sensor conectado
%       0:GP2Y0A41SK0F
%       1:GP2Y0A21YK0F
%       2:GP2Y0A02YK0F
%   numeroPin: identificación del pin.
%   0xB0, CMD = 11, Op, numeroPin, 0, 0, 0, suma, 0x0b


function ret = aNanoPidSensorSet(obj, sensor_type, Op)
  v = [176, 11, sensor_type, Op, 0, 0, 0, 0, 11];

  v(8) = 0;
  for i=1:7
    v(8) = v(8) + v(i);
  endfor
  v(8) = mod(v(8), 256);
  port = obj.serialDev;
  srl_flush(port);
  for i=1:9
    srl_write(port, uint8(v(i)));
  endfor
%  ret =  srl_read(port, 1);
endfunction
