%
%   aNanoPidSetConst(obj,kp, ki, kd)
%   obj: corresponde con el que se establece la comunicación.
%   Kp: Coeficiente proporcional
%   Ke: Factor exp del numero
%   El paquete de datos, para establecer estos valores se construye como:
%   0xB0, CMD = 16, Kp, Ke, 0, 0, 0, suma, 0x0b

function ret = aNanoPidSetKp(obj, kp, ke)
    v = [176, 17, kp, ke, 0, 0, 00, 0, 11];
    printf('Cof Kp = %f y ke = %f \n',kp ,ke);
  v(8) = 0;
  for i=1:7
    v(8) = v(8) + v(i);
  endfor
  v(8) = mod(v(8), 256);
  port = obj.serialDev;
  srl_flush(port);
  for i=1:9
    srl_write(port, uint8(v(i)));
  endfor
  %ret =  srl_read(port, 1);
endfunction
