
%   aNanoPidServoAttach(obj, nroServo, nroPin)
%   obj: corresponde con el que se establece la comunicación.
%   nroPin: identificación del pin al que se conectará a la interrupción.
%   El paquete de datos, para pines digitales, se construye como:
%   0xB0, CMD = 14, nroPin,0, 0, 0, 0, suma, 0x0b


function ret = aNanoPidIntrAttach(obj, nroPin)
  v = [176, 14, nroPin, 0, 0, 0, 0, 0, 11];

  v(8) = 0;
  for i=1:7
    v(8) = v(8) + v(i);
  endfor
  v(8) = mod(v(8), 256);
  port = obj.serialDev;
  srl_flush(port);
  for i=1:9
    srl_write(port, uint8(v(i)));
  endfor
endfunction
