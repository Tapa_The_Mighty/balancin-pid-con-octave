function ret = runApp()
  [dir, name, ext] = fileparts( mfilename('fullpathext') );
  global _pid_guiBasePath = dir;
  global _pid_guiImgPath = [dir filesep() 'img'];
  addpath([dir filesep() "libs" ]);
  addpath([dir filesep() "fcn" ]);
  addpath([dir filesep() "wnd" ]);
  waitfor(mainDlg().figure);
end
