%
%   aNanoPidSetConst(obj,kp, ki, kd)
%   obj: corresponde con el que se establece la comunicación.
%   Kp: Coeficiente proporcional
%   Ki: Coeficiente Ingegral
%   Kd: Coeficiente derivativo
%   t:  Constante de tiempo (t<255)
%   El paquete de datos, para establecer estos valores se construye como:
%   0xB0, CMD = 10, Kp, Ki, Kd, t, 0, suma, 0x0b

%aNanoPidSetConst(mainDlg.aNano_1,sp,cT,grad_offset);
function ret = aNanoPidSetConst(obj, sp, cT, kt, offset)
    v = [176, 10, sp, cT, kt ,offset, 0, 0, 11];

  v(8) = 0;
  for i=1:7
    v(8) = v(8) + v(i);
  endfor
  v(8) = mod(v(8), 256);
  port = obj.serialDev;
  srl_flush(port);
  for i=1:9
    srl_write(port, uint8(v(i)));
  endfor
  %ret =  srl_read(port, 1);
endfunction
