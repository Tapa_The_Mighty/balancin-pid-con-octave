
#include "serialProtocol.h"
#include "SharpIR.h"
#include "PID_v1.h"

protocolData serIn;

void setup() {
  Serial.begin(115200);
  initProtocol(&serIn);
  pinMode(13, OUTPUT);    // sets the digital pin 13 as output
}

void loop() {
  // put your main code here, to run repeatedly:
  ;
}

void serialEvent() {
  while (Serial.available()) 
    decodeProtocol(&serIn, (unsigned char)Serial.read());    
} 
