#include "Arduino.h"
#include <Servo.h>
#include <string.h>
#include "serialProtocol.h"
#include "SharpIR.h"
#include "PID_v1.h"

#define tblSinLen 100

#define SERVO_MIN_VALUE -35
#define SERVO_MAX_VALUE  35


/* Coeficientes del PID: (Ejemplos) */

double  Kp = 0.1,          // Coeficiente P
        Ki = 0.5,          // Coeficiente I
        Kd = 0.025;          // Coeficiente D

int t_const      = 5;    // Cosntante de tiempo (ms)
int offset_servo = 90;   // Offset del Servo
int stop_pin     = 0;    // Pin de STOP

volatile bool RUN_PID = false;

/* Variables del PID */
double  Input    = 0,
        Output   = 0,     //
        Setpoint = 15;  // Punto de reposo

/*No se usan en la version actual*/
double error     = 0,
       lastError = 0,
       cumError  = 0,
       rateError = 0;

unsigned long currentTime  = 0,
              previousTime = 0 ;

double elapsedTime = 0;

/* Definicion de clases */

// Sensor de distancia:
SharpIR *sensor_sharp;

// Servo Motores:
static Servo pid_servo;
static Servo servos[4];

// PID:
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT );

const unsigned int tblSin[tblSinLen] =
{
  0x0200, 0x0220, 0x0240, 0x025f, 0x027f, 0x029e, 0x02bc, 0x02d9, 0x02f6, 0x0312,
  0x032c, 0x0346, 0x035e, 0x0375, 0x038a, 0x039e, 0x03b0, 0x03c0, 0x03cf, 0x03dc,
  0x03e6, 0x03ef, 0x03f6, 0x03fb, 0x03fe, 0x0400, 0x03fe, 0x03fb, 0x03f6, 0x03ef,
  0x03e6, 0x03dc, 0x03cf, 0x03c0, 0x03b0, 0x039e, 0x038a, 0x0375, 0x035e, 0x0346,
  0x032c, 0x0312, 0x02f6, 0x02d9, 0x02bc, 0x029e, 0x027f, 0x025f, 0x0240, 0x0220,
  0x0200, 0x01df, 0x01bf, 0x01a0, 0x0180, 0x0161, 0x0143, 0x0126, 0x0109, 0x00ed,
  0x00d3, 0x00b9, 0x00a1, 0x008a, 0x0075, 0x0061, 0x004f, 0x003f, 0x0030, 0x0023,
  0x0019, 0x0010, 0x0009, 0x0004, 0x0001, 0x0000, 0x0001, 0x0004, 0x0009, 0x0010,
  0x0019, 0x0023, 0x0030, 0x003f, 0x004f, 0x0061, 0x0075, 0x008a, 0x00a1, 0x00b9,
  0x00d3, 0x00ed, 0x0109, 0x0126, 0x0143, 0x0161, 0x0180, 0x01a0, 0x01bf, 0x01df
};


unsigned int getSinValue(void)
{
  static unsigned int idxSin = 0;
  unsigned int ret;
  ret = tblSin[idxSin];
  idxSin++;
  idxSin = idxSin % tblSinLen;
  return ret;
}


/***
   aNanoSetPinType(obj, numeroPin, tipo, argumentos)
   obj: corresponde con el que se establece la comunicación.
   numeroPin: identificación del pin.
   tipo: “digital”
   Los argumentos pueden ser “output”(1),“inputPullUp”(2), “inputFloat” o “input” (3).
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 1, nroPin, tipo 1 - 3, 0, 0, 0, suma, 0x0b
*/

void doCMD01(protocolData *c)
{
  if ((c->data[0] >= 2) && (c->data[0] <= 19))
  {
    if (c->data[1] == 1)
      pinMode(c->data[0], OUTPUT);
    else if (c->data[1] == 2)
      pinMode(c->data[0], INPUT_PULLUP);
    else if (c->data[1] == 3)
      pinMode(c->data[0], INPUT);
  }
}

/***
   aNanoAnalogRead(obj, numeroPin)
   obj: corresponde con el que se establece la comunicación.
   numeroPin: identificación del pin.
   Retorna el valor resultante de la conversión. Los pines que se consideran como válidos son numerados del 14 (A0) al 21 (A7) y el 1 que genera una señal senoidal por software.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 2, nroPin, 0, 0, 0, 0, suma, 0x0b
   Retorna 2 bytes representando el valor resultante de la conversión.
*/
void doCMD02(protocolData *c)
{
  unsigned int val = 0;
  unsigned char b0, b1;
  if (c->data[0] == 1)
    val = getSinValue();
  else if ((c->data[0] >= 14) && (c->data[0] <= 21))
    val = analogRead(c->data[0]);
  b0 = val & 0xFF;
  val = val >> 8;
  b1 = val & 0xFF;
  Serial.write(b0);
  Serial.write(b1);
}

/***
   aNanoDigitalRead(obj, numeroPin)
   obj: corresponde con el que se establece la comunicación.
   numeroPin: identificación del pin.
   Retorna el valor resultante de la conversión. Los pines que se consideran como válidos son numerados del 2 al 19.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 3, nroPin, 0, 0, 0, 0, suma, 0x0b
   Retorna 1 bytes representando el valor presente.
*/

void doCMD03(protocolData *c)
{
  unsigned char val = 0;
  if ((c->data[0] >= 2) && (c->data[0] <= 19))
    val = digitalRead(c->data[0]);
  Serial.write(val);
}

/***
   aNanoAnalogWrite(obj, numeroPin, val)
   obj: corresponde con el que se establece la comunicación.
   numeroPin: identificación del pin.
   Val: un valor de 0 a 255 estableciendo el ciclo útil.
   Los pines válidos para esta función son 3, 5, 6, 9, 10, 11.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 4, nroPin, val, 0, 0, 0, suma, 0x0b
*/
void doCMD04(protocolData *c)
{
  if ((c->data[0] == 3) ||
      (c->data[0] == 5) ||
      (c->data[0] == 6) ||
      (c->data[0] == 9) ||
      (c->data[0] == 10) ||
      (c->data[0] == 11))
    analogWrite(c->data[0], c->data[1]);
}

/***
   aNanoDigitalWrite(obj, numeroPin, val)
   obj: corresponde con el que se establece la comunicación.
   numeroPin: identificación del pin.
   Val: 0 para falso y diferente de cero para cualquier otro valor.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 5, nroPin, val, 0, 0, 0, suma, 0x0b
*/
void doCMD05(protocolData *c)
{
  if ((c->data[0] >= 2) && (c->data[0] <= 19))
  {
    if (c->data[1])
      digitalWrite(c->data[0], HIGH);
    else
      digitalWrite(c->data[0], LOW);
  }
}

/***
   aNanoServoAttach(obj, nroServo, numeroPin)
   obj: corresponde con el que se establece la comunicación.
   numeroPin: identificación del pin al que se conectará el servo.
   nroServo: índice del servo a utilizar. Valores válidos van de 0 a 3 inclusive.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 6, nroServo, nroPin, 0, 0, 0, suma, 0x0b
*/

void doCMD06(protocolData *c)
{
  if ((c->data[0] >= 0) && (c->data[0] <= 3)) {
    servos[c->data[0]].attach(c->data[1]);
  }
}

/***
   aNanoServoDetach(obj, nroServo)
   obj: corresponde con el que se establece la comunicación.
   nroServo: índice del servo a utilizar. Valores válidos van de 0 a 3 inclusive.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 7, nroServo, 0, 0, 0, 0, suma, 0x0b
*/

void doCMD07(protocolData *c)
{
  if ((c->data[0] >= 0) && (c->data[0] <= 3)) {
    servos[c->data[0]].detach();
  }
}

/***
   aNanoServoRead(obj, nroServo)
   obj: corresponde con el que se establece la comunicación.
   nroServo: índice del servo a utilizar. Valores válidos van de 0 a 3 inclusive.
   Retorna la posición actual del servo.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 8, nroServo, 0, 0, 0, 0, suma, 0x0b
*/
void doCMD08(protocolData *c)
{
  unsigned char val = 0;
  if ((c->data[0] >= 0) && (c->data[0] <= 3))
    val = servos[c->data[0]].read();
  Serial.write(val);
}

/***
   aNanoServoWrite(obj, nroServo, pos)
   obj: corresponde con el que se establece la comunicación.
   nroServo: índice del servo a utilizar. Valores válidos van de 0 a 3 inclusive.
   pos: valor de posición angular. Un valor entre 0 y 180.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 9, nroServo, pos, 0, 0, 0, suma, 0x0b
*/
void doCMD09(protocolData *c)
{
  if ((c->data[0] >= 0) && (c->data[0] <= 3))
    servos[c->data[0]].write(c->data[1] + offset_servo);
}

/***
   aNanoPidSetConst(obj,sP, tC, e_tc,offset_servo)
   obj:  corresponde con el que se establece la comunicación.
   sP:   Set Point
   tC:   Constante de tiempo.
   e_tC: Coeficiente del tiempo de muestreo
   offset_servo: Offset del servo
   El paquete de datos, para establecer estos valores se construye como:
   0xB0, CMD = 10, Kp, Ki, Kd, t, Servo_Offset, suma, 0x0b
*/

void doCMD0A(protocolData *c)
{
  Setpoint     = c->data[0];
  if(pow(10,1*c->data[2]) == 0)
  {
    // Ver esto
    t_const = (int)c->data[1] * 100.0;
  }
  else
  {
    t_const = (int)c->data[1] * 1000.0 / pow(10,1*c->data[2]); //t_const = tc * 10^-x
  }
  offset_servo = c->data[3];
}


/***
   aNanoPidSensorSet(obj,sensor_type, Op)
   El paquete de datos, para establecer estos valores se construye como:
   obj: corresponde con el que se establece la comunicación.
   Op: Tipo de sensor conectado
        0:GP2Y0A41SK0F
        1:GP2Y0A21YK0F
        2:GP2Y0A02YK0F
   numeroPin: identificación del pin.
   0xB0, CMD = 11, Op, numeroPin, 0, 0, 0, suma, 0x0b
*/

void doCMD0B(protocolData *c)
{
  int sensor_pin;
  switch (c->data[1]) {
    case 1: {
        sensor_pin = A1;
        break;
      }
    case 2: {
        sensor_pin = A2;
        break;
      }
    case 3: {
        sensor_pin = A3;
        break;
      }
    case 4: {
        sensor_pin = A4;
        break;
      }
    case 5: {
        sensor_pin = A5;
        break;
      }
  }
  switch (c->data[0]) {
    case 1: {
        sensor_sharp = new SharpIR(SharpIR::GP2Y0A41SK0F, sensor_pin );
        break;
      }
    case 2: {
        sensor_sharp = new SharpIR(SharpIR::GP2Y0A21YK0F, sensor_pin );
        break;
      }
    case 3: {
        sensor_sharp = new SharpIR(SharpIR::GP2Y0A02YK0F, sensor_pin );
        break;
      }
  }
}

/*
   aNanoPidRun(obj,0,0,0)
   El paquete de datos, para establecer estos valores se construye como:
   0xB0, CMD = 12, 0, 0, 0, 0, 0, suma, 0x0b
*/

void doCMD0C(protocolData *c)
{
  // Inicializamos variables
  unsigned int val = 0;
  unsigned char b0, b1;

  // Variables PID:
  double dist   = 0;
  double action = 0;
  double val_anterior = sensor_sharp->getDistance();

  // Configuramos PID:
  myPID.SetOutputLimits( -45 , 45 );   // Configuramos limites de la salida
  myPID.SetTunings(Kp, Ki, Kd);        // Sintonizamos los coeficinetes del PID
  myPID.SetSampleTime(t_const);        // Establecemos el timepo de Muestreo en ms
  myPID.SetMode(AUTOMATIC);            // Encendemos el PID.

  myPID.SetControllerDirection(REVERSE);  //Invertimos el accionamiento del PID

  // Encendemos LED para indicar marcha del PID
  digitalWrite(13, HIGH);

  // Bucle del PID:
  while(digitalRead(stop_pin))
  {
    dist = 0;
    //Calculamos la distacia:
    for (int i = 0 ; i < 10; i++)
    {
      dist = dist + sensor_sharp->getDistance();
    }
    //Input = dist / 10.0;
    dist = dist / 10.0;
    if (abs(dist - val_anterior) > 0.5)  //0.5mm de ventana
    {
      val_anterior = dist;  
    }
    Input = val_anterior;
    // Calculamos PID:
    myPID.Compute();

    // Escribimos en servo:
    action = offset_servo - Output;   // Determinamos la accion
    pid_servo.write((int)action);     // <- Ver esto

  }
  //Retornamos la pelota
  pid_servo.write(45);
  delay(800);
  //Ponemos horizontal la barra
  pid_servo.write(90);

  //Apagamos el LED:
  digitalWrite(13, LOW);

  b0 = val & 0xFF;
  val = val >> 8;
  b1 = val & 0xFF;

  Serial.write(b0);
  Serial.write(b1);
}


/***
   aNanoPidServoAttach(obj, numeroPin)
   obj: corresponde con el que se establece la comunicación.
   numeroPin: identificación del pin al que se conectará el servo.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 13, nroPin,0, 0, 0, 0, suma, 0x0b
*/

void doCMD0D(protocolData * c)
{
  pid_servo.attach(c->data[0]);
}

/***
   aNanoPidIntrAttach(obj, nroPin)
   obj: corresponde con el que se establece la comunicación.
   nroPin: identificación del pin al que se conectará a la interrupción.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 14, nroPin,0, 0, 0, 0, suma, 0x0b
*/

void doCMD0E(protocolData * c)
{
  /* No se usa en la aplicacion actual*/
  stop_pin = c->data[0];
  pinMode(stop_pin,INPUT_PULLUP);
}

/***
   aNanoPidSensorRead(obj)
   obj: corresponde con el que se establece la comunicación.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 15, 0, 0, 0, 0, 0, suma, 0x0b
   Retorna la distancia medida por el sensor
*/
void doCMD0F(protocolData * c)
{
  unsigned int val = 0;
  unsigned char b0, b1;
  for (int i = 0 ; i < 10; i++) {
    val = val + sensor_sharp->getDistance();
  }
  val = val / 10;

  b0 = val & 0xFF;
  val = val >> 8;
  b1 = val & 0xFF;
  Serial.write(b0);
  Serial.write(b1);
}

/***
   aNanoPidServoOffset(obj, offset)
   offset:Ajuste del servo en grados
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 15, Servo_Offset ,0, 0, 0, 0, suma, 0x0b
*/

void doCMD10(protocolData * c)
{
  if (c->data[0] > 0 && c->data[0] < 180)
  {
    offset_servo  = c->data[0];
  }
}

/***
   aNanoPidSetConstKP(obj, kp, ke)
   offset:Ajuste del servo
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 17, kp, ke, 0, 0, 0, suma, 0x0b
*/

void doCMD11(protocolData * c)
{
  if (c->data[1] == 0 )
  {
    Kp = c->data[0];
  }
  else
  {
    Kp = c->data[0] * pow(10, -1 * c->data[1]); // Kp = kp * 10^-ke
  }
}

/***
   aNanoPidSetConstKI(obj, ki, ke)
   offset:Ajuste del servo
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 18, kp, ke, 0, 0, 0, suma, 0x0b
*/

void doCMD12(protocolData * c)
{
  if (c->data[1] == 0 )
  {
    Ki = c->data[0];
  }
  else
  {
    Ki = c->data[0] * pow(10, -1 * c->data[1]); // Ki = ki * 10^-ke
  }
}

/***
   aNanoPidSetConstKP(obj, kd, ke)
   offset:Ajuste del servo
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 19, kp, ke, 0, 0, 0, suma, 0x0b
*/

void doCMD13(protocolData * c)
{
  if (c->data[1] == 0 )
  {
    Kd = c->data[0];
  }
  else
  {
    Kd = c->data[0] * pow(10, -1 * c->data[1]); // Kd = kd * 10^-ke
  }
}


const doCMD cmdActionList[20] =
{
  doCMD01,    //  0
  doCMD02,    //  1
  doCMD03,    //  2
  doCMD04,    //  3
  doCMD05,    // ...
  doCMD06,
  doCMD07,
  doCMD08,
  doCMD09,
  doCMD0A,    // PID set Cof
  doCMD0B,    // PID Sensor Set
  doCMD0C,    // PID RUN
  doCMD0D,    // PID Servo Attach
  doCMD0E,    // PID INTR Attach
  doCMD0F,    // PID Sensor Read
  doCMD10,    // PID Servo Offset
  doCMD11,    // PID set Cof Kp
  doCMD12,    // PID set Cof Ki
  doCMD13     // PID set Cof Kd
};

//
// Detecta el encabezado
//

void stageHeader(protocolData * b, unsigned char val)
{
  if (val == 0xB0)
  {
    b->crc = 0xB0;
    b->dataInx = 0;
    b->nextState = STATE_CMD;
  }
  else
    b->nextState = STATE_HEADER;
}
//
// Carga CMD
//
void stageCMD(protocolData * b, unsigned char val)
{
  if ((val >= FIRST_CMD) && (val <= LAST_CMD))
  {
    b->crc += val;
    b->cmd = val;
    b->nextState = STATE_DATA;
  }
  else
    b->nextState = STATE_HEADER;
}

//
// Carga payload
//
void stageDATA(protocolData * b, unsigned char val)
{
  b->data[b->dataInx] = val;
  b->crc += val;
  b->dataInx ++;
  if (b->dataInx == 5)
    b->nextState = STATE_CRC;
}

//
// verifica CRC (ojo, no incluye el marcador de fin de paquete 0x0B
//
void stageCRC(protocolData * b, unsigned char val)
{
  if (b->crc == val)
    b->nextState = STATE_FOOTER;
  else
    b->nextState = STATE_HEADER;
}

//
// Verifica fin de paquete y ejecuta la orden asociada.
//
void stageFOOTER(protocolData * b, unsigned char val)
{
  if (val == 0x0B)
  {
    if ((b->cmd >= FIRST_CMD) && (b->cmd <= LAST_CMD))
      cmdActionList[b->cmd - FIRST_CMD](b);
  }
  b->nextState = STATE_HEADER;
}


const stageFunc allStages[5] =
{
  stageHeader, // 0
  stageCMD,    // 1
  stageDATA,   // 2
  stageCRC,    // 3
  stageFOOTER  // 4
};

void initProtocol(protocolData * p)
{
  memset(p, 0, sizeof(protocolData));
}

void decodeProtocol(protocolData * p, unsigned char newVal)
{
  allStages[p->nextState](p, newVal);
}

/* Funciones del PID */
/* No se usa en esta version.*/
float computePID(double Input) {
  currentTime = millis();                               // obtener el tiempo actual
  elapsedTime = (double)(currentTime - previousTime);   // calcular el tiempo transcurrido

  error = Setpoint - Input;                             // determinar el error entre la consigna y la medición
  cumError += error * elapsedTime;                      // calcular la integral del error
//  if(elapsedTime != 0)
//  {
  rateError = (error - lastError) / elapsedTime;        // calcular la derivada del error
//  }

  float output = (Kp * error) + (Ki * cumError) + (Kd * rateError); // calcular la salida del PID

  lastError = error;                                    // almacenar error anterior
  previousTime = currentTime;                           // almacenar el tiempo anterior

  return output;                                        // devolvemos la salida del servo
}
