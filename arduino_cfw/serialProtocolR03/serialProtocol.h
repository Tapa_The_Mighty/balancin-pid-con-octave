#ifndef SERIALPROTOCOL
#define SERIALPROTOCOL

#define STATE_HEADER 0
#define STATE_CMD    1
#define STATE_DATA   2
#define STATE_CRC    3
#define STATE_FOOTER 4

#define FIRST_CMD 0x01

#define CMD_SET_PIN_TYPE          0x01
#define CMD_ANALOG_READ           0x02
#define CMD_DIGITAL_READ          0x03
#define CMD_ANALOG_WRITE          0x04
#define CMD_DIGITAL_WRITE         0x05
#define CMD_SERVO_ATTACH          0x06
#define CMD_SERVO_DETACH          0x07
#define CMD_SERVO_READ            0x08
#define CMD_SERVO_WRITE           0x09
#define CMD_PID_SET_COEF          0x0A
#define CMD_PID_S_SET             0x0B
#define CMD_PID_RUN               0x0C
#define CMD_PID_SERVO_ATTACH      0x0D
#define CMD_PID_INTR_ATTACH       0x0E
#define CMD_PID_S_READ            0x0F
#define CMD_PID_S_OFFSET          0x10
#define CMD_PID_SET_COEF_KP       0x11
#define CMD_PID_SET_COEF_KI       0x12
#define CMD_PID_SET_COEF_KD       0x13


#define LAST_CMD CMD_PID_SET_COEF_KD

#define bit00 0x01
#define bit01 0x02
#define bit02 0x04
#define bit03 0x08
#define bit04 0x10
#define bit05 0x20
#define bit06 0x40
#define bit07 0x80


typedef struct
{
  unsigned char cmd;
  unsigned char data[5];
  unsigned char dataInx;
  unsigned char crc;
  unsigned char nextState;
}protocolData;

typedef void (*stageFunc)(protocolData *, unsigned char);

void stageHeader(protocolData* b, unsigned char val);
void stageCMD(protocolData* b, unsigned char val);
void stageDATA(protocolData* b, unsigned char val);
void stageCRC(protocolData* b, unsigned char val);
void stageFOOTER(protocolData* b, unsigned char val);

typedef void (*doCMD)(protocolData*);

void doCMD01(protocolData *);
void doCMD02(protocolData *);
void doCMD03(protocolData *);
void doCMD04(protocolData *);
void doCMD05(protocolData *);
void doCMD06(protocolData *);
void doCMD07(protocolData *);
void doCMD08(protocolData *);
void doCMD09(protocolData *);
void doCMD0A(protocolData *);
void doCMD0B(protocolData *);
void doCMD0C(protocolData *);
void doCMD0D(protocolData *);
void doCMD0E(protocolData *);
void doCMD0F(protocolData *);
void doCMD10(protocolData *);
void doCMD11(protocolData *);
void doCMD12(protocolData *);
void doCMD13(protocolData *);

void initProtocol(protocolData *);
void decodeProtocol(protocolData *, unsigned char newVal);

unsigned int getSinValue(void);
float computePID(double inp);
void pid_run_stop();

#endif
